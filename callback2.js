
const lists = require('./Data/lists.json');

module.exports = function (searchID, callback) {
        
    setTimeout(() => {            
                    
        const keyArray = Object.keys(lists);
        let check = false;            
            
        //check if any listID is same as user passed listID  
    
        keyArray.forEach((key)=>{

            if(key === searchID){
                
                callback(lists[key]);
                check = true;
            }
        });
            
        if(check == false){

            console.log('No such ID Exist');
        }

    }, 2 * 1000);
}


