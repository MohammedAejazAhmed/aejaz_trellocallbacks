
const cards = require('./Data/cards.json');

module.exports = function (searchID, callback) {
	
    setTimeout(() => {    
            
        const keyArray = Object.keys(cards);            
        let check = true;
            
        //check if listID is present in cards array            

        keyArray.forEach((property)=>{

            if(property === searchID){
                    
                callback(cards[property]);
                check = false;
            }
        });
            
        if(check){
    
            console.log('No such ID exist');
        }             
    }, 2 * 1000);
}

