
const boards = require('./test/testCallback1.js');
const lists = require('./test/testCallback2.js');
const cards = require('./test/testCallback3.js');
let boardsID;
let listID

const callback6 = function(){

    setTimeout(()=>{
        
        //Printing board information from Thanos board 

        boards((id)=>{
            
            boardsID = id;
        }); 
        
        lists((id)=>{
            
            //Printing list information from List data

            listID = id;
            
            //Printing card information from card data 
           
            listID.forEach((element)=>{

                cards(element['id']);
            });
        }); 
    }, 2 * 1000);
}

callback6();
