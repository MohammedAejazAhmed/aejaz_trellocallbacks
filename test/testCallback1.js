const callback1 = require('./../callback1.js');

function testCallback (callback, boardID ="def453ed"){

    callback1(boardID ,(name, id)=>{
        
       console.log("id: "+id);
       console.log("name: "+name);
    
       callback(id); 
    });
}

/*default call
testCallback();*/

module.exports = testCallback;
