
const callback2 = require('./../callback2.js');

function testCallback (callback, boardID = 'def453ed'){

    callback2(boardID, (listData)=>{

        console.log(listData);
        callback(listData);
    });
}

/*default Call
testCallback2();*/

module.exports = testCallback;
