const callback3 = require('./../callback3.js');

function testCallback(listID = 'qwsa221'){

    callback3(listID, (cardData)=>{

        console.log(cardData);
    });
}

/*default call
testCallback3();*/

module.exports = testCallback;

